﻿using System;
using System.Linq;
using System.IO;
using System.Reflection;

namespace ConsoleApp1
{
    class Program
    {
        static string path = @"results.txt";

        static bool checkPrimeNumber(int number)
        {
            bool isPrime = true;
            if (number > 2)
            {
                for (int i = 2; i < number / 2 + 1; i++)
                {
                    if (number % i == 0)
                        isPrime = false;
                }
            }
            else if (number < 2)
                isPrime = false;

            return isPrime;
        }

        static void savePrimeNumber(int number)
        {
            StreamWriter sw = File.AppendText(path);

            sw.WriteLine(number.ToString());

            sw.Close();
        }

        static int getLastSavedPrime()
        {
            string last = getLastFileLine();

            int number = 0;
            bool success = Int32.TryParse(last, out number);
            if (!success)
            {
                Console.WriteLine($"Couldn't parse {last} as int. Press any key to close program.");
                Console.ReadKey();
                Environment.Exit(0);
            }


            return Int32.Parse(last);
        }

        static string getLastFileLine()
        {
            string last = "";

            try
            {
                last = File.ReadLines(path).Last();
                Console.WriteLine("Read " + last + " from results.txt file");
            }
            catch
            {
                last = "1";
                Console.WriteLine("File results.txt found, but it's empty. Creating starting falue");
            }

            return last;
        }

        static void Main(string[] args)
        {
            if (!File.Exists(path))
            {
                File.CreateText(path).Close();
                Console.WriteLine("Created results.txt in path:");
                Console.WriteLine(Assembly.GetExecutingAssembly().Location);
            }

            int number = getLastSavedPrime();

            Console.WriteLine("Press any key to start generator. To stop it just close console");
            Console.ReadKey();
            while (true)
            {
                number++;

                if (checkPrimeNumber(number))
                {
                    savePrimeNumber(number);
                    Console.WriteLine("Saved " + number + " as a prime number.");
                }
                else
                {
                    Console.WriteLine(number + " is not a prime number.");
                }

            }
        }
    }
}
